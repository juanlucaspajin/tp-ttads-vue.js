# Trabajo final TTADS

## Overview

Este es el trabajo final de la materia Técnicas y Tecnologías Avanzadas de desarrollo de Software.

Este trata sobre un juego multijugador de pelea, en cual se podrán crear y editar personajes y luchar contra los personajes de otro jugador en tiempo real.
Además, cuenta con perfil de administrador, el cual podrá manipular los ataques de los personajes.

## Tecnologías incluídas

### Front-End

- Node.js
- Axios
- Vue.js
- VueX
- Vue Router
- Vue Socket io
- Vue Persistedstate
- Js-cookie
- FontAwesome
- Bootstrap 4
- HTML 5
- CSS 3

### Back-end

- Node.js
- Express.js
- Sequelize
- BCrypt.js
- Json Web Token (jwt)
- MySql
- Socket.io


## Integrantes del grupo de desarrollo

- Corti, Tomás
- Ghiotti, Martín
- Pajin, Juan Lucas