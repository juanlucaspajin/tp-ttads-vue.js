const express = require('express')
const router = express.Router()
const cors = require('cors')

const Lobbie = require('../models/Lobbie')
const Fight = require('../models/Fight')
router.use(cors())

process.env.SECRET_KEY = 'secret'

router.post('/create',(req,res) => {
	const fightBody = {
		id_personaje1: req.body.id_personaje1,
		id_personaje2: null,
		vida_personaje1: req.body.vida_personaje1,
		vida_personaje2: null,
		energia_personaje1: req.body.energia_personaje1,
		energia_personaje2: null,
		turno: 1,
		id_usuario1: req.body.id_usuario1,
		id_usuario2: null
	}
	const lobbie = {
		descripcion_lobbie: req.body.descripcion_lobbie,
		estado: 'created',
		id_pelea: null
	}

	Fight.create(fightBody)
	.then(fight => {
		lobbie.id_pelea = fight.id_pelea;
		Lobbie.create(lobbie)
		.then(lob => {
			//res.json({status: 'Lobbie created'})
			res.json(lob)
		})
		.catch(err => {
			res.status(500).send('Error: ' + err)
		})
	})
	.catch(err => {
		res.send('Error: ' + err)
	})

	
})

router.post('/finish/:id_pelea',(req,res) => {
	const id_fight = req.params.id_pelea;
	Lobbie.findOne({
		where: {
			id_pelea: id_fight
		}
	})
	.then(lobby => {
		lobby.update({
			estado: 'finished'
		})
		res.json(lobby)
	})
	.catch(err => {
		res.status(500).send('Error: ' + err)
	})
})

router.get('/get-all',(req,res) => {
	Lobbie.findAll({
		where: {
			estado: 'created'
		}
	})
	.then(lobbies => {
		res.json(lobbies)
	})
	.catch(err => {
		res.status(500).send('Error: ' + err)
	})
})

module.exports = router