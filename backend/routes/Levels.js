const express = require('express')
const router = express.Router()
const cors = require('cors')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')

const Level = require('../models/Level')
router.use(cors())

process.env.SECRET_KEY = 'secret'

router.get('/getAll',(req,res) =>{
    Level.findAll()
    .then(levels => {
		res.json(levels);
	})
	.catch(err => {
		res.status(500).send('Error: ' + err)
	})
})

module.exports = router