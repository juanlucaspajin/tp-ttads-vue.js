const express = require('express')
const router = express.Router()
const cors = require('cors')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
// const SocketIO = require('socket.io');

const Fight = require('../models/Fight')
const Character = require('../models/Character')
router.use(cors())
const { Op } = require('sequelize')


process.env.SECRET_KEY = 'secret'

router.post('/add', (req, res) => {
	const fightBody = {
		id_personaje1: req.body.id_personaje1,
		id_personaje2: req.body.id_personaje2,
		vida_personaje1: req.body.vida_personaje1,
		vida_personaje2: req.body.vida_personaje2,
		energia_personaje1: req.body.energia_personaje1,
		energia_personaje2: req.body.energia_personaje2,
		turno: 1,
		id_usuario1: '',
		id_usuario2: ''
	}

	Fight.create(fightBody)
		.then(fight => {
			res.json(fight)
		})
		.catch(err => {
			res.status(500).send('Error: ' + err)
		})

})

router.post('/changeturn/:id_pelea', (req, res) => {
	const id_fight = req.params.id_pelea;
	Fight.findOne({
		where: {
			id_pelea: id_fight
		}
	})
		.then(fight => {
			if (fight) {
				if (fight.turno == 1) {
					fight.update({
						turno: 2
					})
				}
				else {
					fight.update({
						turno: 1
					})
				}
			}
			res.json(fight.turno);
		})
		.catch(err => {
			res.status(500).send('Error: ' + err)
		})

})

router.post('/attack', (req, res) => {
	const fightBody = {
		id_fight: req.body.id_fight,
		attack_energy: req.body.attack_energy,
		evadio: req.body.evadio
	}
	Fight.findOne({
		where: {
			id_pelea: fightBody.id_fight
		}
	})
		.then(fight => {
			if (fight) {
				if (fight.turno == 1) {
					if (!fightBody.evadio) {
						fight.update({
							vida_personaje2: fight.vida_personaje2 - fightBody.attack_energy,
							energia_personaje1: fight.energia_personaje1 - fightBody.attack_energy
						})
					} else {
						fight.update({
							energia_personaje1: fight.energia_personaje1 - fightBody.attack_energy
						})
					}
				}
				else {
					if (!fightBody.evadio) {
						fight.update({
							vida_personaje1: fight.vida_personaje1 - fightBody.attack_energy,
							energia_personaje2: fight.energia_personaje2 - fightBody.attack_energy
						})
					} else {
						fight.update({
							energia_personaje2: fight.energia_personaje2 - fightBody.attack_energy
						})
					}
				}
			}
			res.json(fight)
		})
		.catch(err => {
			res.send('Error: ' + err)
		})

})

router.post('/defense', (req, res) => {
	const fightBody = {
		id_fight: req.body.id_fight,
		character_defense: Math.round(req.body.character_defense * 0.5),
		character_life: req.body.character_life,
		character_energy: req.body.character_energy
	}
	Fight.findOne({
		where: {
			id_pelea: fightBody.id_fight
		}
	})
		.then(fight => {
			if (fight) {
				vida_personaje = 0
				energia_personaje = 0
				if (fight.turno == 1) {
					if ((fight.vida_personaje1 + fightBody.character_defense) > fightBody.character_life) {
						vida_personaje = fightBody.character_life
					}
					else {
						vida_personaje = fight.vida_personaje1 + fightBody.character_defense
					}

					if ((fight.energia_personaje1 + fightBody.character_defense) > fightBody.character_energy) {
						energia_personaje = fightBody.character_energy
					}
					else {
						energia_personaje = fight.energia_personaje1 + fightBody.character_defense
					}
					fight.update({
						vida_personaje1: vida_personaje,
						energia_personaje1: energia_personaje
					})
				}
				else {
					if ((fight.vida_personaje2 + fightBody.character_defense) > fightBody.character_life) {
						vida_personaje = fightBody.character_life
					}
					else {
						vida_personaje = fight.vida_personaje2 + fightBody.character_defense
					}

					if ((fight.energia_personaje2 + fightBody.character_defense) > fightBody.character_energy) {
						energia_personaje = fightBody.character_energy
					}
					else {
						energia_personaje = fight.energia_personaje2 + fightBody.character_defense
					}
					fight.update({
						vida_personaje2: vida_personaje,
						energia_personaje2: energia_personaje
					})
				}
			}
			res.json(fight)
		})
		.catch(err => {
			res.status(500).send('Error: ' + err)
		})

})

router.get('/fight/:id_fight', (req, res) => {
	var id_fight = req.params.id_fight;

	Fight.findOne({
		where: {
			id_pelea: id_fight
		}
	})
		.then(fight => {
			res.json(fight)
		})
		.catch(err => {
			res.status(500).send('Error: ' + err)
		})
})

router.post('/updateFight', (req, res) => {
	const id_pelea_lobby = req.body.id_pelea;
	const id_personaje2 = req.body.id_personaje2;
	const id_usuario = req.body.id_usuario2;

	Character.findOne({
		where: {
			id_personaje: id_personaje2
		}
	})
		.then(char => {
			Fight.findOne({
				where: {
					id_pelea: id_pelea_lobby
				}
			})
				.then(pelea => {
					if (pelea) {
						pelea.update({
							id_personaje2: char.id_personaje,
							vida_personaje2: char.vida,
							energia_personaje2: char.energia,
							id_usuario2: id_usuario
						})
						res.json(pelea);
					} else {
						res.status(500).send('Error: Pelea no encontrada')
					}
				})
				.catch(err2 => {
					res.status(500).send('Error: ' + err2)
				})
		})
		.catch(err => {
			res.status(500).send('Error: ' + err)
		})

})



module.exports = router;