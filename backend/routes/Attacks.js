const express = require('express')
const router = express.Router()
const cors = require('cors')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
// const SocketIO = require('socket.io');

const Attack = require('../models/Attack')
const CharacterAttacks = require('../models/CharacterAttacks')
router.use(cors())
const { Op } = require('sequelize')

process.env.SECRET_KEY = 'secret'

function isAuthenticated(req, res, next) {
	var user = jwt.decode((req.headers["authorization"]));

	if (user && user.id_usuario) {
		return next();
	}
	
	res.status(500).send('Error: No esta autorizado para ingresar a esa ruta')
}

router.get('/attack/:id_attack', (req, res) => {
	var id_attack = req.params.id_attack;

	Attack.findOne({
		where: {
			id_ataque: id_attack
		}
	})
		.then(attack => {
			res.json(attack)
		})
		.catch(err => {
			res.send('Error: ' + err)
		})
})
//Rest API Attacks
//Get attacks
router.get('/', isAuthenticated, (req, res) => {
	Attack.findAll()
		.then(attacks => {
			res.json(attacks)
		})
		.catch(err => {
			res.status(500).send('Error: ' + err)
		})
});
//Update attack
router.put('/:id_ataque', isAuthenticated, (req, res) => {
	var id_atk = req.params.id_ataque

	Attack.findOne({
		where: {
			id_ataque: id_atk
		}
	})
		.then(ataque => {
			ataque.update({
				nombre_ataque: req.body.nombre_ataque,
				energia_requerida: req.body.energia_requerida
			})
				.then(response => {
					res.json({ status: 'El ataque ha sido modificado' })
				})
				.catch(error => {
					res.send(error)
				})
		})
		.catch(err => {
			res.status(500).send('Error: ' + err)
		})
});
//Create attack
router.post('/', isAuthenticated, (req, res) => {
	const ataqueBody = {
		nombre_ataque: req.body.nombre_ataque,
		energia_requerida: req.body.energia_requerida
	}
	if (ataqueBody.nombre_ataque == '') {
		res.status(500).send('El nombre no puede estar vacío');
	}
	if (ataqueBody.energia_requerida == 0) {
		res.status(500).send('La energia requerida no puede ser 0');
	}
	Attack.findOne({
		where: {
			nombre_ataque: ataqueBody.nombre_ataque,
		}
	}).then(ataque => {
		if (ataque == null) {
			Attack.create(ataqueBody)
				.then(response => {
					res.json({ status: 'El ataque ha sido creado' })
				})
				.catch(err => {
					res.send('Error: ' + err)
				})
		} else {
			res.status(500).send('Nombre de ataque existente');
		}
	})
})
//Delete attack
router.delete('/:id_ataque', isAuthenticated, (req, res) => {
	var id_atk = req.params.id_ataque

	Attack.destroy({
		where: {
			id_ataque: id_atk
		}
	})
		.then(ataque => {
			res.json({ status: 'El ataque ha sido eliminado' })
		})
		.catch(err => {
			res.status(500).send('Error: ' + err)
		})
})


module.exports = router;