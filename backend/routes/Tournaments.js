const express = require('express')
const tournaments = express.Router()
const cors = require('cors')

const Tournament = require('../models/Tournament')
tournaments.use(cors())

tournaments.get('/tournaments/:id_torneo', (req,res) => {
	var id_torneo = req.params.id_torneo;

	Tournament.findOne({
        where: {
            id_torneo: id_torneo
        } 
    })
    .then(tournament => {
    	res.json(tournament)
    })
    .catch(err => {
    	res.status(500).send('Error: ' + err)
    })
})

module.exports = tournaments;