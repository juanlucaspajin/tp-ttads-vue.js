const express = require('express')
const router = express.Router()
const cors = require('cors')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')

const User = require('../models/User')
const UserCharacter = require('../models/UserCharacter')
const Character = require('../models/Character')
const CharacterTypes = require('../models/CharacterType')
const CharacterAttacks = require('../models/CharacterAttacks')
const Attack = require('../models/Attack')
const Level = require('../models/Level')
const { Op } = require('sequelize')
router.use(cors())

process.env.SECRET_KEY = 'secret'

router.get('/my-characters/:id_usuario', (req, res) => {

	var id_user = req.params.id_usuario

	UserCharacter.findAll({
		where: {
			id_usuario: id_user
		}
	})
		.then(array_characters => {
			var arr = [];

			for (var i = 0; i < array_characters.length; i++) {
				arr.push(array_characters[i].id_personaje)
			}

			Character.findAll({
				where: {
					id_personaje: arr
				}
			})
				.then(character => {
					res.json(character);
				})
				.catch(err => {
					res.status(500).send('Error: ' + err)
				})

		})
		.catch(err => {
			res.status(500).send('Error: ' + err)
		})
})

router.get('/character/:id_personaje', (req, res) => {
	var id_personaje = req.params.id_personaje;

	Character.findOne({
		where: {
			id_personaje: id_personaje
		}
	})
		.then(character => {
			res.json(character)
		})
		.catch(err => {
			res.status(500).send('Error: ' + err)
		})
})

router.post('/character/delete/:id_personaje', (req, res) => {
	var id_pj = req.params.id_personaje;

	Character.destroy({
		where: {
			id_personaje: id_pj
		}
	}).then(character => {
		res.json({ status: 'El personaje ha sido borrado' })
	}).catch(err => {
		res.status(500).send('Error: ' + err)
	})
})

router.post('/character/levelup/:id_personaje', (req, res) => {
	var id_personaje = req.params.id_personaje;

	Character.findOne({
		where: {
			id_personaje: id_personaje
		}
	})
		.then(character => {
			var new_experience = (character.experiencia + 100)
			var current_level = character.id_nivel
			var character_type = character.id_tipo_personaje

			Level.max('nro_nivel',
			{
				where: {
					experiencia_minima: {
						[Op.lte]: new_experience
					}
				}
			})
			.then(max => {
				Level.max('id_nivel',
				{
					where: {
						nro_nivel: max
					}		
				})
				.then(level => {
					character.update({
						experiencia: new_experience,
						id_nivel: level
					})
					if (current_level != level)
					{
						CharacterTypes.findOne({
							where: {
								id_tipo_personaje: character_type
							}
						})
						.then(type => {
							character.update({
								vida: (type.porcentaje_vida * 100 * max),
								energia: (type.porcentaje_energia * 100 * max),
								defensa: (type.porcentaje_defensa * 100 * max),
								evasion: (type.porcentaje_evasion * 100 * max)
							})
						})
					}
				res.json({ status: 'El personaje ha sido leveleado' })
				})

			})
		})
		.catch(err => {
			res.status(500).send('Error: ' + err)
		})
})

router.get('/character-types', (req, res) => {
	CharacterTypes.findAll()
		.then(character_types => {
			res.json(character_types);
		})
		.catch(err => {
			res.status(500).send('Error: ' + err)
		})
})

router.post('/createCharacter', (req, res) => {
	const id_usuario = req.body.id_usuario;
	const characterData = {
		nombre: req.body.nombre,
		vida: req.body.vida,
		energia: req.body.energia,
		defensa: req.body.defensa,
		evasion: req.body.evasion,
		id_nivel: req.body.id_nivel,
		id_tipo_personaje: req.body.id_tipo_personaje,
		experiencia: req.body.experiencia,
		id_usuario: req.body.id_usuario
	};
	Character.findOne({
		where: {
			nombre: req.body.nombre,
		}
	}).then(character => {
		if (!character) {
			Character.create(characterData)
				.then(character => {

					const userCharacterData = {
						id_usuario: id_usuario,
						id_personaje: character.id_personaje
					}
					UserCharacter.create(userCharacterData)
						.then(userCharacter => {
							var attacks = req.body.selectedAttacks
							const characterAttacks = []
							attacks.forEach(att =>
								characterAttacks.push({
									id_ataque: att,
									id_personaje: userCharacter.id_personaje
								}))
							CharacterAttacks.bulkCreate(characterAttacks)
								.then(asds => {
									res.json({ status: 'Creado' });
								})
								.catch(err3 => {
									res.status(500).send('Error: ' + err3)
								})
						}).catch(err2 => {
							res.status(500).send('Error: ' + err3)
						})

				})
				.catch(err => {
					res.status(500).send('Error: ' + err)
				})
		} else {
			res.status(500).send('Personaje existente');
		}
	})
		.catch(err => {
			res.status(500).send('Error: ' + err)
		})
})

router.post('/get-attacks-by-energy', (req, res) => {
	const id_character = req.body.id_character
	const energia = req.body.energia

	CharacterAttacks.findAll({
		where: {
			id_personaje: id_character
		}
	})
		.then(characterAttacks => {
			const characterAttacksID = []
			characterAttacks.forEach(att =>
				characterAttacksID.push(att.id_ataque))
			Attack.findAll({
				where: {
					id_ataque: characterAttacksID,
					energia_requerida: {
						[Op.lte]: energia
					}
				}
			})
				.then(attacks => {
					res.json(attacks)
				})
				.catch(err => {
					res.status(500).send('Error: ' + err)
				})
		})
		.catch(err => {
			res.status(500).send('Error: ' + err)
		})
});

//Ataques del personaje (solo los ID)
router.post('/attacks', (req, res) => {
	var id_per = req.body.id_personaje

	CharacterAttacks.findAll({
		where: {
			id_personaje: id_per
		}
	})
		.then(attacks => {
			res.json(attacks)
		})
		.catch(err => {
			res.status(500).send('Error: ' + err)
		})
});

router.post('/add-attacks', (req, res) => {
	var id_per = req.body.id_personaje;
	var attacks = req.body.attacks;
	var insertedRows = []

	attacks.forEach((element, index) => {
		var row = {
			id_ataque: element,
			id_personaje: id_per
		};
		insertedRows.push(row)
	});

	CharacterAttacks.bulkCreate(insertedRows)
		.then(attacks => {
			res.json("Ataques creados")
		})
		.catch(err => {
			res.status(500).send('Error: ' + err)
		})
})

module.exports = router;