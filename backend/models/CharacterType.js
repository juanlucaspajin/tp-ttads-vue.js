const Sequelize = require('sequelize');
const db = require('../database/db');

module.exports = db.sequelize.define(
    'tipo_personaje',
    {
        id_tipo_personaje: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        descripcion_tipo_personaje: {
            type: Sequelize.STRING
        },
        porcentaje_vida: {
            type: Sequelize.FLOAT
        },
        porcentaje_energia: {
            type: Sequelize.FLOAT
        },
        porcentaje_defensa: {
            type: Sequelize.FLOAT
        },
        porcentaje_evasion: {
            type: Sequelize.FLOAT
        }
    },
    {
        timestamps: false,
        freezeTableName: true
    }
)
