const Sequelize = require('sequelize');
const db = require('../database/db');

module.exports = db.sequelize.define(
    'nivel',
    {
        id_nivel: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        nro_nivel: {
            type: Sequelize.INTEGER
        },
        experiencia_minima: {
            type: Sequelize.INTEGER
        },
        puntos_totales: {
            type: Sequelize.INTEGER
        }
    },
    {
        timestamps: false,
        freezeTableName: true
    }
)
