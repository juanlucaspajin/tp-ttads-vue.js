const Sequelize = require('sequelize');
const db = require('../database/db');

module.exports = db.sequelize.define(
    'lobbie',
    {
        id_lobbie: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        descripcion_lobbie: {
            type: Sequelize.STRING
        },
        estado: {
            type: Sequelize.INTEGER
        },
        id_pelea: {
            type: Sequelize.INTEGER
        }
    },
    {
        timestamps: false,
        freezeTableName: true
    }
)