const Sequelize = require('sequelize');
const db = require('../database/db');

module.exports = db.sequelize.define(
    'torneo',
    {
        id_torneo: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        id_usuario_personaje: {
            type: Sequelize.INTEGER
        }
    },
    {
        timestamps: false,
        freezeTableName: true
    }
)
