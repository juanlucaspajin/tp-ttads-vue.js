const Sequelize = require('sequelize');
const db = require('../database/db');

module.exports = db.sequelize.define(
    'ataque',
    {
        id_ataque: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        nombre_ataque: {
            type: Sequelize.STRING
        },
        energia_requerida: {
            type: Sequelize.INTEGER
        }
    },
    {
        timestamps: false,
        freezeTableName: true
    }
)
