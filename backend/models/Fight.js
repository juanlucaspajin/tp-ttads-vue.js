const Sequelize = require('sequelize');
const db = require('../database/db');

module.exports = db.sequelize.define(
    'pelea',
    {
        id_pelea: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        id_personaje1: {
            type: Sequelize.INTEGER
        },
        id_personaje2: {
            type: Sequelize.INTEGER
        },
        vida_personaje1: {
            type: Sequelize.INTEGER
        },
        vida_personaje2: {
            type: Sequelize.INTEGER
        },
        energia_personaje1: {
            type: Sequelize.INTEGER
        },
        energia_personaje2: {
            type: Sequelize.INTEGER
        },
        turno: {
            type: Sequelize.INTEGER
        },
        id_usuario1: {
            type: Sequelize.INTEGER
        },
        id_usuario2: {
            type: Sequelize.INTEGER
        }
    },
    {
        timestamps: false,
        freezeTableName: true
    }
)
