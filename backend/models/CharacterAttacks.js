const Sequelize = require('sequelize');
const db = require('../database/db');

module.exports = db.sequelize.define(
    'personaje_ataque',
    {
        id_personaje_ataque: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        id_ataque: {
            type: Sequelize.INTEGER
        },
        id_personaje: {
            type: Sequelize.INTEGER
        }
    },
    {
        timestamps: false,
        freezeTableName: true
    }
)
