const express = require('express');
const mysql = require('mysql');
const cors = require('cors');
var bodyParser = require('body-parser');
var socket = require('socket.io');
const app = express();

app.use(bodyParser.json({type: 'application/json'}));
app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));

var conn = mysql.createConnection({
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: 'root',
    database: 'db_tp_java'
});

const server = app.listen(5000, function(){
    console.log("Server on port: ",5000);
    var host = server.address().address;
    var port = server.address().port;
});

conn.connect(function (error) {
    if (!error) {
        console.log('connected!');
    } else {
        console.log('Error: ' + error);
    }
});


var io = socket(server);
//SOCKETS ------------------------------------


io.on('connection', function(socket) {
    
    socket.on('message', function(data){
        var data2 = {
            message: data.message,
            id: socket.id
        }
        io.sockets.emit('message',data2)
    })

    socket.on('pelea_union', function(data){
        
        io.sockets.emit('pelea_union_response',data)
    })

    socket.on('pelea_ataque', function(data){
        
        io.sockets.emit('pelea_ataque_response',data)
    })

    socket.on('pelea_defensa', function(data){
        
        io.sockets.emit('pelea_defensa_response',data)
    })
});

//--------------------------------------------

var Users = require('./routes/Users');
app.use('/users',Users);

var Characters = require('./routes/Characters');
app.use('/user/characters', Characters);

var Levels = require('./routes/Levels');
app.use('/levels', Levels);

var Attacks = require('./routes/Attacks');
app.use('/attacks', Attacks);

var Tournaments = require('./routes/Tournaments');
app.use('/tournaments', Tournaments);

var Lobbies = require('./routes/Lobbies');
app.use('/lobbies', Lobbies);

var Fights = require('./routes/Fights');
app.use('/fights', Fights);

