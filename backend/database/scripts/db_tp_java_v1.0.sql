-- MySQL dump 10.13  Distrib 5.6.23, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: db_tp_java
-- ------------------------------------------------------
-- Server version	5.5.60

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ataque`
--

DROP TABLE IF EXISTS `ataque`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ataque` (
  `id_ataque` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_ataque` varchar(45) NOT NULL,
  `energia_requerida` int(11) NOT NULL,
  PRIMARY KEY (`id_ataque`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ataque`
--

LOCK TABLES `ataque` WRITE;
/*!40000 ALTER TABLE `ataque` DISABLE KEYS */;
INSERT INTO `ataque` VALUES (1,'Fireball',15),(2,'Killer punch',25),(3,'Power suck',30),(4,'Kamehameha',45),(5,'Genkidama',50),(6,'Masenko',20),(7,'Basic punch',5);
/*!40000 ALTER TABLE `ataque` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `combate`
--

DROP TABLE IF EXISTS `combate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `combate` (
  `id_combate` int(11) NOT NULL AUTO_INCREMENT,
  `id_enemigo` int(11) NOT NULL,
  `experiencia` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_combate`),
  KEY `personaje_combate_idx` (`id_enemigo`),
  CONSTRAINT `personaje_combate` FOREIGN KEY (`id_enemigo`) REFERENCES `personaje` (`id_personaje`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `combate`
--

LOCK TABLES `combate` WRITE;
/*!40000 ALTER TABLE `combate` DISABLE KEYS */;
INSERT INTO `combate` VALUES (1,4,100),(2,2,200),(3,3,300);
/*!40000 ALTER TABLE `combate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nivel`
--

DROP TABLE IF EXISTS `nivel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nivel` (
  `id_nivel` int(11) NOT NULL AUTO_INCREMENT,
  `nro_nivel` int(11) NOT NULL,
  `experiencia_minima` int(11) NOT NULL,
  `puntos_totales` int(11) NOT NULL,
  PRIMARY KEY (`id_nivel`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nivel`
--

LOCK TABLES `nivel` WRITE;
/*!40000 ALTER TABLE `nivel` DISABLE KEYS */;
INSERT INTO `nivel` VALUES (1,1,0,100),(2,2,100,200),(3,3,300,300),(4,4,500,400);
/*!40000 ALTER TABLE `nivel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partida`
--

DROP TABLE IF EXISTS `partida`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partida` (
  `id_partida` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `id_torneo` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_partida`),
  KEY `partida_torneo_idx` (`id_torneo`),
  KEY `partida_usuario_idx` (`id_usuario`),
  CONSTRAINT `partida_torneo` FOREIGN KEY (`id_torneo`) REFERENCES `torneo` (`id_torneo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `partida_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partida`
--

LOCK TABLES `partida` WRITE;
/*!40000 ALTER TABLE `partida` DISABLE KEYS */;
INSERT INTO `partida` VALUES (1,'test','2019-08-17 15:33:24',49,1),(2,'test 2','2019-08-18 18:25:18',49,1),(3,'paliza 1','2019-08-25 02:22:05',50,4),(4,'Paliza 2','2019-08-26 18:46:13',56,1),(5,'Paliza 3','2019-08-26 19:31:12',61,1),(6,'Intermedio partida','2019-08-26 20:13:57',76,1),(7,'test','2019-09-01 18:37:34',80,1),(8,'test 3','2019-09-01 18:51:24',81,1),(9,'test 4','2019-09-01 18:58:59',84,1);
/*!40000 ALTER TABLE `partida` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personaje`
--

DROP TABLE IF EXISTS `personaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personaje` (
  `id_personaje` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `vida` int(11) DEFAULT NULL,
  `energia` int(11) DEFAULT NULL,
  `defensa` int(11) DEFAULT NULL,
  `evasion` int(11) DEFAULT NULL,
  `experiencia` int(11) DEFAULT NULL,
  `id_nivel` int(11) DEFAULT NULL,
  `id_tipo_personaje` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_personaje`),
  KEY `personaje_nivel_idx` (`id_nivel`),
  KEY `personaje_tipo_idx` (`id_tipo_personaje`),
  CONSTRAINT `personaje_nivel` FOREIGN KEY (`id_nivel`) REFERENCES `nivel` (`id_nivel`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `personaje_tipo` FOREIGN KEY (`id_tipo_personaje`) REFERENCES `tipo_personaje` (`id_tipo_personaje`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personaje`
--

LOCK TABLES `personaje` WRITE;
/*!40000 ALTER TABLE `personaje` DISABLE KEYS */;
INSERT INTO `personaje` VALUES (1,'Magoevon',100,100,100,100,3200,4,1),(2,'Saruman',10,60,50,20,0,1,1),(3,'Carlos',15,110,80,25,0,1,2),(4,'pepe',5,80,120,40,300,4,2),(5,'Mugricio',80,80,40,200,300,4,3),(6,'Pedro',40,20,30,10,0,1,2),(7,'Raul el patovica',120,60,90,30,300,3,2),(8,'Kakaroto',100,100,100,100,700,4,1),(9,'Isildur',25,25,25,25,0,1,1),(10,'Pajita',160,80,120,40,2000,4,2),(11,'Naruto',40,20,30,10,0,1,2),(12,'Superman',40,20,30,10,0,1,2);
/*!40000 ALTER TABLE `personaje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personaje_ataque`
--

DROP TABLE IF EXISTS `personaje_ataque`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personaje_ataque` (
  `id_personaje_ataque` int(11) NOT NULL AUTO_INCREMENT,
  `id_personaje` int(11) NOT NULL,
  `id_ataque` int(11) NOT NULL,
  PRIMARY KEY (`id_personaje_ataque`),
  KEY `personaje_personajeAtaque_idx` (`id_personaje`),
  KEY `ataque_personajeAtaque_idx` (`id_ataque`),
  CONSTRAINT `ataque_personajeAtaque` FOREIGN KEY (`id_ataque`) REFERENCES `ataque` (`id_ataque`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `personaje_personajeAtaque` FOREIGN KEY (`id_personaje`) REFERENCES `personaje` (`id_personaje`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personaje_ataque`
--

LOCK TABLES `personaje_ataque` WRITE;
/*!40000 ALTER TABLE `personaje_ataque` DISABLE KEYS */;
INSERT INTO `personaje_ataque` VALUES (1,1,2),(2,2,1),(3,2,6),(4,3,6),(5,4,1),(6,4,6),(7,1,1),(8,1,7),(9,4,2),(10,2,2),(11,3,2),(12,3,3),(13,2,3),(14,2,4),(15,3,5),(16,5,1),(17,5,6),(18,5,7),(19,7,1),(20,7,6),(21,7,7),(22,8,1),(23,8,2),(24,8,6),(25,8,7),(26,9,1),(27,9,2),(28,9,6),(29,9,7),(30,10,1),(31,10,6),(32,10,7),(33,4,3),(34,4,7),(35,11,1),(36,11,6),(37,11,7);
/*!40000 ALTER TABLE `personaje_ataque` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_personaje`
--

DROP TABLE IF EXISTS `tipo_personaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_personaje` (
  `id_tipo_personaje` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion_tipo_personaje` varchar(45) NOT NULL,
  `porcentaje_vida` float DEFAULT NULL,
  `porcentaje_energia` float DEFAULT NULL,
  `porcentaje_defensa` float DEFAULT NULL,
  `porcentaje_evasion` float DEFAULT NULL,
  PRIMARY KEY (`id_tipo_personaje`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_personaje`
--

LOCK TABLES `tipo_personaje` WRITE;
/*!40000 ALTER TABLE `tipo_personaje` DISABLE KEYS */;
INSERT INTO `tipo_personaje` VALUES (1,'Mago',0.25,0.25,0.25,0.25),(2,'Heroe',0.4,0.2,0.3,0.1),(3,'Ladron',0.2,0.2,0.1,0.5);
/*!40000 ALTER TABLE `tipo_personaje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `torneo`
--

DROP TABLE IF EXISTS `torneo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `torneo` (
  `id_torneo` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario_personaje` int(11) NOT NULL,
  PRIMARY KEY (`id_torneo`),
  KEY `torneo_usuarioPersonaje_idx` (`id_usuario_personaje`),
  CONSTRAINT `torneo_usuarioPersonaje` FOREIGN KEY (`id_usuario_personaje`) REFERENCES `usuario_personaje` (`id_usuario_personaje`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `torneo`
--

LOCK TABLES `torneo` WRITE;
/*!40000 ALTER TABLE `torneo` DISABLE KEYS */;
INSERT INTO `torneo` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(27,1),(28,1),(29,1),(30,1),(31,1),(32,1),(33,1),(34,1),(35,1),(36,1),(37,1),(38,1),(39,1),(40,1),(41,1),(42,1),(43,1),(44,1),(45,1),(46,1),(47,1),(83,1),(84,1),(85,1),(86,1),(87,1),(88,1),(89,1),(90,1),(93,1),(94,1),(48,4),(49,4),(77,4),(50,5),(92,5),(51,6),(91,6),(52,7),(53,7),(54,7),(55,7),(56,8),(60,8),(61,8),(75,8),(76,8),(57,9),(58,9),(59,9),(74,9),(78,9),(79,9),(80,9),(81,9),(62,10),(63,10),(64,10),(65,10),(66,10),(67,10),(68,10),(69,10),(70,10),(71,10),(72,10),(73,10),(82,10);
/*!40000 ALTER TABLE `torneo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `torneo_combate`
--

DROP TABLE IF EXISTS `torneo_combate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `torneo_combate` (
  `id_torneo_combate` int(11) NOT NULL AUTO_INCREMENT,
  `id_torneo` int(11) NOT NULL,
  `id_combate` int(11) NOT NULL,
  `combate_activo` tinyint(4) DEFAULT NULL,
  `id_siguiente_combate` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_torneo_combate`),
  KEY `fk_torneo_torneoCombate_idx` (`id_torneo`),
  KEY `fk_combate_torneoCombate_idx` (`id_combate`),
  KEY `fk_torneoCombate_torneoCombate_idx` (`id_siguiente_combate`),
  CONSTRAINT `fk_combate_torneoCombate` FOREIGN KEY (`id_combate`) REFERENCES `combate` (`id_combate`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_torneoCombate_torneoCombate` FOREIGN KEY (`id_siguiente_combate`) REFERENCES `torneo_combate` (`id_torneo_combate`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_torneo_torneoCombate` FOREIGN KEY (`id_torneo`) REFERENCES `torneo` (`id_torneo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=220 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `torneo_combate`
--

LOCK TABLES `torneo_combate` WRITE;
/*!40000 ALTER TABLE `torneo_combate` DISABLE KEYS */;
INSERT INTO `torneo_combate` VALUES (2,2,1,1,NULL),(3,3,1,1,NULL),(4,4,1,1,NULL),(5,5,1,1,NULL),(7,20,2,0,NULL),(8,21,1,1,NULL),(9,21,2,0,NULL),(10,21,3,0,NULL),(11,24,3,0,NULL),(12,24,2,0,NULL),(13,25,3,0,NULL),(14,25,2,0,NULL),(15,25,1,1,NULL),(16,27,3,0,NULL),(17,27,2,0,16),(18,27,1,1,17),(19,28,3,0,NULL),(20,28,2,0,19),(21,28,1,1,20),(22,29,3,0,NULL),(23,29,2,0,22),(24,29,1,0,23),(25,30,3,0,NULL),(26,30,2,0,25),(27,30,1,1,26),(28,31,3,0,NULL),(29,31,2,0,28),(30,31,1,1,29),(31,32,3,0,NULL),(32,32,2,0,31),(33,32,1,1,32),(34,33,3,0,NULL),(35,33,2,1,34),(36,33,1,0,35),(37,34,3,0,NULL),(38,34,2,0,37),(39,34,1,1,38),(40,35,3,0,NULL),(41,35,2,0,40),(42,35,1,1,41),(43,36,3,1,NULL),(44,36,2,0,43),(45,36,1,0,44),(46,37,3,1,NULL),(47,37,2,0,46),(48,37,1,0,47),(49,38,3,0,NULL),(50,38,2,1,49),(51,38,1,0,50),(52,39,3,0,NULL),(53,39,2,1,52),(54,39,1,0,53),(55,40,3,0,NULL),(56,40,2,1,55),(57,40,1,0,56),(58,41,3,0,NULL),(59,41,2,1,58),(60,41,1,0,59),(61,42,3,0,NULL),(62,42,2,1,61),(63,42,1,0,62),(64,43,3,1,NULL),(65,43,2,0,64),(66,43,1,0,65),(67,44,3,0,NULL),(68,44,2,0,67),(69,44,1,1,68),(70,45,3,0,NULL),(71,45,2,1,70),(72,45,1,0,71),(73,46,3,0,NULL),(74,46,2,0,73),(75,46,1,1,74),(76,47,3,1,NULL),(77,47,2,0,76),(78,47,1,0,77),(79,48,3,0,NULL),(80,48,2,0,79),(81,48,1,1,80),(82,49,3,0,NULL),(83,49,2,0,82),(84,49,1,1,83),(85,50,3,0,NULL),(86,50,2,0,85),(87,50,1,1,86),(88,51,3,0,NULL),(89,51,2,0,88),(90,51,1,1,89),(91,52,3,0,NULL),(92,52,2,0,91),(93,52,1,1,92),(94,53,3,0,NULL),(95,53,2,0,94),(96,53,1,1,95),(97,54,3,0,NULL),(98,54,2,0,97),(99,54,1,1,98),(100,55,3,1,NULL),(101,55,2,0,100),(102,55,1,0,101),(103,56,3,0,NULL),(104,56,2,1,103),(105,56,1,0,104),(106,57,3,0,NULL),(107,57,2,0,106),(108,57,1,1,107),(109,58,3,0,NULL),(110,58,2,0,109),(111,58,1,1,110),(112,59,3,0,NULL),(113,59,2,0,112),(114,59,1,1,113),(115,60,3,0,NULL),(116,60,2,1,115),(117,60,1,0,116),(118,61,3,1,NULL),(119,61,2,0,118),(120,61,1,0,119),(121,62,3,0,NULL),(122,62,2,0,121),(123,62,1,1,122),(124,63,3,0,NULL),(125,63,2,1,124),(126,63,1,0,125),(127,64,3,0,NULL),(128,64,2,1,127),(129,64,1,0,128),(130,65,3,0,NULL),(131,65,2,1,130),(132,65,1,0,131),(133,66,3,0,NULL),(134,66,2,1,133),(135,66,1,0,134),(136,67,3,0,NULL),(137,67,2,1,136),(138,67,1,0,137),(139,68,3,1,NULL),(140,68,2,0,139),(141,68,1,0,140),(142,69,3,1,NULL),(143,69,2,0,142),(144,69,1,0,143),(145,70,3,0,NULL),(146,70,2,1,145),(147,70,1,0,146),(148,71,3,0,NULL),(149,71,2,1,148),(150,71,1,0,149),(151,72,3,0,NULL),(152,72,2,1,151),(153,72,1,0,152),(154,73,3,1,NULL),(155,73,2,0,154),(156,73,1,0,155),(157,74,3,0,NULL),(158,74,2,0,157),(159,74,1,1,158),(160,75,3,0,NULL),(161,75,2,1,160),(162,75,1,0,161),(163,76,3,0,NULL),(164,76,2,1,163),(165,76,1,0,164),(166,77,3,1,NULL),(167,77,2,0,166),(168,77,1,0,167),(169,78,3,0,NULL),(170,78,2,0,169),(171,78,1,1,170),(172,79,3,0,NULL),(173,79,2,0,172),(174,79,1,1,173),(175,80,3,0,NULL),(176,80,2,0,175),(177,80,1,1,176),(178,81,3,0,NULL),(179,81,2,0,178),(180,81,1,1,179),(181,82,3,0,NULL),(182,82,2,0,181),(183,82,1,1,182),(184,83,3,0,NULL),(185,83,2,0,184),(186,83,1,1,185),(187,84,3,0,NULL),(188,84,2,1,187),(189,84,1,0,188),(190,85,3,0,NULL),(191,85,2,1,190),(192,85,1,0,191),(193,86,3,0,NULL),(194,86,2,1,193),(195,86,1,0,194),(196,87,3,1,NULL),(197,87,2,0,196),(198,87,1,0,197),(199,88,3,1,NULL),(200,88,2,0,199),(201,88,1,0,200),(202,89,3,0,NULL),(203,89,2,1,202),(204,89,1,0,203),(205,90,3,1,NULL),(206,90,2,0,205),(207,90,1,0,206),(208,91,3,0,NULL),(209,91,2,0,208),(210,91,1,1,209),(211,92,3,1,NULL),(212,92,2,0,211),(213,92,1,0,212),(214,93,3,1,NULL),(215,93,2,0,214),(216,93,1,0,215),(217,94,3,0,NULL),(218,94,2,0,217),(219,94,1,1,218);
/*!40000 ALTER TABLE `torneo_combate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(45) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `apellido` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `fechaCreacion` datetime DEFAULT NULL,
  `rol` varchar(45) NOT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'juan','456','Juan','Perez','juan@mail.com','2019-06-27 00:00:00','admin'),(2,'tomasito','$2b$10$8HRFALaWj/QNFXl6nZM0j.n5cy02wE76ulH/gvrMROxyTH5RcITD6','Tomas','Corti','test@test.com','2019-07-20 01:00:20','admin'),(3,'react','$2b$10$yO4yESHo5R3LsrBJXgv3r.Kqq4RNJfG.IlpuOnk68/EU5m9uzqNeO','react','test','react@test.com','2019-07-20 04:38:27','admin'),(4,'rolfi','1234','Lucas','Monteverde','rolaxxx@hotmail.com','2019-08-25 02:19:33','usuario'),(5,'martin','$2b$10$Ew4trD7lsJk6Ot5fFVnWlOHZaSptkZIVGHUs7.Az1dm2H4HumI5Qe','Martin','Ghiotti','test3@test.com','2019-09-08 17:43:41','admin'),(6,'mbu','$2b$10$3KNY3MNJYRQ2seBg2FGWuOC6f01iLUoLB328PiWSIRoDIG6yopb5q','MB','UTN','mbu@utn.frro.edu.ar','2019-10-21 21:12:33','usuario'),(7,'test 3','$2b$10$Raiy.5opX9BXa7ckSHEZnuQ6oLuFsn6NZu0hdh889JLCzJfeEqXsO','test','test','test@test.com','2019-10-21 21:40:03','usuario'),(8,'martin2','$2b$10$cL3MjUkH5rYsRYS/ShReo.9Q2nLAzHHYxfsNEkbZk4Hp/rzTHAo9W','tycasc','uaygvsas','asdas@asdasd.com','2019-10-21 21:42:30','usuario');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario_personaje`
--

DROP TABLE IF EXISTS `usuario_personaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario_personaje` (
  `id_usuario_personaje` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `id_personaje` int(11) NOT NULL,
  PRIMARY KEY (`id_usuario_personaje`),
  KEY `usuario_usuarioPersonaje_idx` (`id_usuario`),
  KEY `personaje_usuarioPersonaje_idx` (`id_personaje`),
  CONSTRAINT `personaje_usuarioPersonaje` FOREIGN KEY (`id_personaje`) REFERENCES `personaje` (`id_personaje`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `usuario_usuarioPersonaje` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario_personaje`
--

LOCK TABLES `usuario_personaje` WRITE;
/*!40000 ALTER TABLE `usuario_personaje` DISABLE KEYS */;
INSERT INTO `usuario_personaje` VALUES (1,1,1),(2,1,2),(3,1,3),(4,1,4),(5,4,5),(6,4,6),(7,4,7),(8,1,8),(9,6,9),(10,1,10),(11,1,11),(12,6,12);
/*!40000 ALTER TABLE `usuario_personaje` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-11 19:04:30
