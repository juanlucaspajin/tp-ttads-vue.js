import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import jwt_decode from 'jwt-decode'
import createPersistedState from 'vuex-persistedstate'
// import Cookies from 'js-cookie'

Vue.use(Vuex)

const config = {
	headers: { 'Authorization': localStorage.access_token }
};

export default new Vuex.Store({
	state: {
		token: localStorage.getItem('access_token') || null,
		character: {},
		enemy: {},
		turno: 1
	},
	getters: {
		loggedIn(state) {
			return state.token != null
		},
		isAdmin() {
			const tok = localStorage.getItem('access_token')
			if (tok != null) {
				var user = jwt_decode(tok)
				if (user.rol == 'admin') {
					return true
				} else {
					return false
				}
			} else {
				return false
			}
		},
		character(state) {
			return state.character
		},
		enemy(state) {
			return state.enemy
		}
	},
	mutations: {
		retrieveToken(state, token) {
			state.token = token
		},
		turn(state,newTurn){
			state.turno = newTurn
		},
		destroyToken(state) {
			state.token = null
		},
		retrieveCharacter(state, character) {
			state.character = character
		},
		retrieveEnemy(state, enemy) {
			state.enemy = enemy
		}
	},

	actions: {
		changeStateTurn(context,newTurn){
			context.commit('turn', newTurn);
		},
		updateCharacter(context, newCharacter){
			context.commit('retrieveCharacter', newCharacter)
		},
		updateEnemy(context,newEnemy){
			context.commit('retrieveEnemy', newEnemy)
		},
		retrieveToken(context, credentials) {
			return new Promise((resolve, reject) => {
				axios
					.post('http://localhost:5000/users/login', {
						usuario: credentials.usuario,
						password: credentials.password
					})
					.then(res => {
						//localStorage.setItem('usertoken',res.data)
						const token = res.data
						localStorage.setItem('access_token', token)
						context.commit('retrieveToken', token)
						resolve(res)
						// this.$router.push({ path: '/profile' });
						// return res.data
					})
					.catch(err => {
						reject(err)
					})
			})
		},
		destroyToken(context) {
			if (context.getters.loggedIn) {
				return new Promise((resolve) => {

					localStorage.removeItem('access_token')
					context.commit('destroyToken')
					resolve(true);

				})
			}
		},
		retrieveCharacter(context, id_personaje) {
			return new Promise((resolve, reject) => {
				axios
					.get('http://localhost:5000/user/characters/character/' + id_personaje)
					.then(res => {

						const character = res.data
						localStorage.setItem('character', character.id_personaje)
						context.commit('retrieveCharacter', character.id_personaje)
						resolve(res)

					})
					.catch(err => {
						reject(err)
					})
			})
		},
		//Test method to retrieve a dummy enemy (personaje with id = 1)
		retrieveEnemy(context) {
			return new Promise((resolve, reject) => {
				axios
					.get('http://localhost:5000/user/characters/character/' + 1)
					.then(res => {

						const enemy = res.data
						localStorage.setItem('enemy', enemy.id_personaje)
						context.commit('retrieveEnemy', enemy.id_personaje)
						resolve(res)

					})
					.catch(err => {
						reject(err)
					})
			})
		},
		registerUser(context, user) {
			return new Promise((resolve, reject) => {
				axios
					.post('http://localhost:5000/users/register', {
						usuario: user.usuario,
						password: user.password,
						nombre: user.nombre,
						apellido: user.apellido,
						email: user.email,
						rol: user.rol
					})
					.then(res => {
						resolve(res)
						return res
					})
					.catch(err => {
						reject(err)
					})
			})
		},
		getUser(context) {
			return new Promise((resolve, reject) => {
				if (context.getters.loggedIn) {
					const token = localStorage.getItem('access_token')
					var user = jwt_decode(token)
					resolve(user)
				} else {
					reject(false)
				}

			})
		},
		getUserCharacter(context) {
			return new Promise((resolve, reject) => {
				if (context.getters.loggedIn) {
					const character = localStorage.getItem('character')
					resolve(character)
				} else {
					reject(false)
				}

			})
		},
		getUserEnemy(context) {
			return new Promise((resolve, reject) => {
				if (context.getters.loggedIn) {
					const enemy = localStorage.getItem('enemy')
					resolve(enemy)
				} else {
					reject(false)
				}

			})
		},
		getCharacter(context, id_personaje) {
			return new Promise((resolve, reject) => {
				axios
					.get('http://localhost:5000/user/characters/character/' + id_personaje)
					.then(res => {
						resolve(res.data)
					})
					.catch(err => {
						reject(err)
					})
			})
		},

		getFight(context, id_fight) {
			return new Promise((resolve, reject) => {
				axios
					.get('http://localhost:5000/fights/fight/' + id_fight)
					.then(res => {
						resolve(res.data)
					})
					.catch(err => {
						reject(err)
					})
			})
		},

		getAttack(context, id_attack) {
			return new Promise((resolve, reject) => {
				axios
					.get('http://localhost:5000/attacks/attack/' + id_attack, config, {})
					.then(res => {
						resolve(res.data)
					})
					.catch(err => {
						reject(err)
					})
			})
		},


		getAllCharactersOfUser(context, id_user) {
			return new Promise((resolve, reject) => {
				axios
					.get('http://localhost:5000/user/characters/my-characters/' + id_user)
					.then(res => {
						resolve(res.data)
					})
					.catch(err => {
						reject(err)
					})
			})
		},

		getAllCharactersTypes() {
			return new Promise((resolve, reject) => {
				axios
					.get('http://localhost:5000/user/characters/character-types')
					.then(res => {
						resolve(res.data)
					})
					.catch(err => {
						reject(err)
					})
			})
		},
		createNewFight(context, fightObj) {
			return new Promise((resolve, reject) => {
				axios
					.post('http://localhost:5000/fights/add', fightObj)
					.then(res => {
						resolve(res)
						return res
					})
					.catch(err => {
						reject(err)
					})
			})
		},

		changeTurn(context, id_fight) {
			return new Promise((resolve, reject) => {
				axios
					.post('http://localhost:5000/fights/changeturn/'+ id_fight)
					.then(res => {
						//Supongo que trae el turno cambiado
						context.commit('turn', res.data);
						resolve(res)
						return res
					})
					.catch(err => {
						reject(err)
					})
			})
		},
		attack(context, fightObj) {
			return new Promise((resolve, reject) => {
				axios
					.post('http://localhost:5000/fights/attack', fightObj)
					.then(res => {
						resolve(res)
						return res
					})
					.catch(err => {
						reject(err)
					})
			})
		},

		defense(context, fightObj) {
			return new Promise((resolve, reject) => {
				axios
					.post('http://localhost:5000/fights/defense', fightObj)
					.then(res => {
						resolve(res)
						return res
					})
					.catch(err => {
						reject(err)
					})
			})
		},
		getLevels() {
			return new Promise((resolve, reject) => {
				axios
					.get('http://localhost:5000/levels/getAll')
					.then(res => {
						resolve(res.data)
					})
					.catch(err => {
						reject(err)
					})
			})
		},
		createCharacter(context, character) {
			return new Promise((resolve, reject) => {
				axios
					.post('http://localhost:5000/user/characters/createCharacter', {
						nombre: character.nombre,
						vida: character.vida,
						defensa: character.defensa,
						energia: character.energia,
						evasion: character.evasion,
						id_nivel: character.id_nivel,
						id_tipo_personaje: character.id_tipo_personaje,
						experiencia: character.experiencia,
						id_usuario: character.id_usuario,
						selectedAttacks: character.attacks
					})
					.then(res => {
						resolve(res.data)
					})
					.catch(err => {
						reject(err);
					})
			})
		},
		deleteCharacter(context, id_personaje) {
			return new Promise((resolve, reject) => {
				axios
					.post('http://localhost:5000/user/characters/character/delete/' + id_personaje)
					.then(res => {
						resolve(res)
						return res
					})
					.catch(err => {
						reject(err)
					})
			})
		},

		levelUpCharacter(context, id_personaje) {
			return new Promise((resolve, reject) => {
				axios
					.post('http://localhost:5000/user/characters/character/levelup/' + id_personaje)
					.then(res => {
						resolve(res)
						return res
					})
					.catch(err => {
						reject(err)
					})
			})
		},
		//Rest API Attacks
		getAllAttacks() {
			return new Promise((resolve, reject) => {
				axios
					.get('http://localhost:5000/attacks/', config)
					.then(res => {
						resolve(res)
						return res
					})
					.catch(err => {
						reject(err)
					})
			})
		},
		updateAttack(context, attackObj) {
			return new Promise((resolve, reject) => {
				axios
					.put('http://localhost:5000/attacks/' + attackObj.id_ataque, attackObj, config)
					.then(res => {
						resolve(res)
						return res
					})
					.catch(err => {
						reject(err)
					})
			})
		},
		createNewAttack(context, attackObj) {
			return new Promise((resolve, reject) => {
				axios
					.post('http://localhost:5000/attacks/', attackObj, config)
					.then(res => {
						resolve(res)
						return res
					})
					.catch(err => {
						reject(err)
					})
			})
		},
		deleteAttack(context, id_attack) {
			return new Promise((resolve, reject) => {
				axios
					.delete('http://localhost:5000/attacks/' + id_attack, config)
					.then(res => {
						resolve(res)
						return res
					})
					.catch(err => {
						reject(err)
					})
			})
		},
		getAttacksByCharacterEnergy(context, characterEnergy) {
			return new Promise((resolve, reject) => {
				axios
					.post('http://localhost:5000/user/characters/get-attacks-by-energy', {
						id_character: characterEnergy.id_character,
						energia: characterEnergy.energy
					})
					.then(res => {
						resolve(res)
						return res
					})
					.catch(err => {
						reject(err)
					})
			})
		},
		getLobbies() {
			return new Promise((resolve, reject) => {
				axios
					.get('http://localhost:5000/lobbies/get-all')
					.then(res => {
						resolve(res)
						return res
					})
					.catch(err => {
						reject(err)
					})
			})
		},
		createNewLobbie(context, pelea_lobby) {
			return new Promise((resolve, reject) => {
				axios
					.post('http://localhost:5000/lobbies/create', pelea_lobby)
					.then(res => {
						resolve(res)
						return res
					})
					.catch(err => {
						reject(err)
					})
			})
		},
		updateFightLobby(context, body) {
			return new Promise((resolve, reject) => {
				axios
					.post('http://localhost:5000/fights/updateFight', body)
					.then(res => {
						resolve(res)
						return res
					})
					.catch(err => {
						reject(err)
					})
			})
		},
		finishFight(context,id_pelea){
			return new Promise((resolve,reject) =>{
				axios
					.post('http://localhost:5000/lobbies/finish/' + id_pelea)
					.then(res => { 
						resolve(res);
						return res 
					})
					.catch(err => {
						reject(err)
					})
			})
		},
		getAttacksOfCharacter(context, personaje){
			return new Promise((resolve,reject) =>{
				axios
					.post('http://localhost:5000/user/characters/attacks', personaje)
					.then(res => { 
						resolve(res);
						return res 
					})
					.catch(err => {
						reject(err)
					})
			})
		},
		insertNewAttacksOfCharacters(context,attacks) {
			return new Promise((resolve,reject) =>{
				axios
					.post('http://localhost:5000/user/characters/add-attacks', attacks)
					.then(res => { 
						resolve(res);
						return res 
					})
					.catch(err => {
						reject(err)
					})
			})
		}
	},
	modules: {
	},
	plugins: [
		createPersistedState({ storage: window.sessionStorage })
	]
})
