import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'
import VueSocketIO from 'vue-socket.io'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'

Vue.config.productionTip = false

router.beforeEach((to,from,next) => {
	if (to.matched.some(record => record.meta.requiresAuth)) {
		if (!store.getters.loggedIn) {
			next({
				path: '/login',
				query: {redirect: to.fullPath}
			})
		} else {
			next()
		}
	} else {
		next()
	}
})

Vue.use(new VueSocketIO({
    debug: true,
    connection: 'http://localhost:5000', //options object is Optional
    vuex: {
      store,
      actionPrefix: "SOCKET_",
      mutationPrefix: "SOCKET_"
    }
  })
);

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
