import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import Profile from '@/components/Profile'
import Logout from '@/components/Logout'
import Register from '@/components/Register'
import Characters from '@/components/views/Characters'
import ABMCharacters from '@/components/views/ABMCharacters'
import Attacks from '@/components/views/Attacks'
import Menu from '@/components/views/Menu'
import MenuTorneo from '@/components/views/MenuTorneo'
import TorneoPersonaje from '@/components/views/TorneoPersonaje'
import CargaTorneo from '@/components/views/CargaTorneo'
import Combate from '@/components/views/Combate'
import LobbyTorneos from '@/components/views/LobbyTorneos'
import MensajeError from '@/components/MensajeError'
import CombateTerminado from '@/components/views/CombateTerminado'


Vue.use(Router)


export default new Router({
	routes: [
		{
			path: '/login',
			name: 'Login',
			component: Login
		},
		{
			path: '/logout',
			name: 'Logout',
			component: Logout
		},
		{
			path: '/profile',
			name: 'Profile',
			component: Profile,
			meta: {
				requiresAuth: true
			}
		},
		{
			path: '/my-characters/:id_usuario',
			name: 'Characters',
			props: true,
			component: Characters,
			meta: {
				requiresAuth: true
			}
		},
		{
			path: '/character/:modo/:id_personaje?',
			name: 'ABMCharacters',
			props: true,
			component: ABMCharacters,
			meta: {
				requiresAuth: true
			}
		},
		{
			path: '/register',
			name: 'Register',
			component: Register
		},
		{
			path: '/attacks',
			name: 'Atacks',
			component: Attacks,
			meta: {
				requiresAuth: true
			}
		},
		{
			path: '/',
			name: 'Menu',
			component: Menu,
			meta: {
				requiresAuth: true
			}
		},
		{
			path: '/menutorneo',
			name: 'MenuTorneo',
			component: MenuTorneo,
			meta: {
				requiresAuth: true
			}
		},
		{
			path: '/torneopersonaje',
			name: 'TorneoPersonaje',
			component: TorneoPersonaje,
			meta: {
				requiresAuth: true
			}
		},
		{
			path: '/cargatorneo',
			name: 'CargaTorneo',
			component: CargaTorneo,
			meta: {
				requiresAuth: true
			}
		},
		{
			path: '/lobby',
			name: 'Lobby torneos',
			component: LobbyTorneos,
			meta: {
				requiresAuth: true
			}
		},
		{
			path: '/combate/:id_pelea',
			name: 'Combate',
			component: Combate,
			props: true,
			meta: {
				requiresAuth: true
			}
		},
		{
			path: '/error',
			name: 'Error',
			component: MensajeError,
			props: true
		},
		{
			path: '/combateterminado/:id_pelea',
			name: 'CombateTerminado',
			component: CombateTerminado,
			props: true
		}
	]
})